## UpdatePocketmine.sh

This script will update a running Pocketmine server to the latest Pocketmine Release on Github.

It uses the mcstatus utility (see below) to ensure an update only occurs when no players are online.

You can either run this script manually, or configure it to run at an automated interval.

I have tested this on OSX Yosemite - no idea on whether this works for any other OS setup.

## Requirements
make sure you install the following otherwise this script won't work:

 - curl

 - wget (brew install wget)

 - mcstatus (pip install mcstatus)

 - growlnotify (http://growl.info/downloads)
 
 - jq (brew install jq)
