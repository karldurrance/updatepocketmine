#!/bin/sh

## UpdatePocketmine.sh
## v1.10
## This script will update to the latest Pocketmine build on the PMMP Github Releases page
## It will not perform an update if there are active players on the server
## Either run this script manually, or configure it to run at an automated interval
## Tested on OSX Yosemite, should work generically???

## Karl Durrance, June 2017

## Requires:
## - curl
## - wget (brew install wget)
## - mcstatus (pip install mcstatus)
## - growlnotify (http://growl.info/downloads)
## - jq (brew install jq)

################################################################################################################
## Config (change these settings to your configuration):

## this is the address of Pocketmine server, normally will be localhost
minecraftIP="localhost"

## this is the PORT of Pocketmine server, normally will be 19132
minecraftPort="19132"

## Github URL
releaseURL="https://api.github.com/repos/pmmp/PocketMine-MP/releases/latest"

## this is a temporary state file, just put it anywhere - suggest Documents in Home dir
stateFile="/Users/karldurrance/Documents/Minecraftbuild.txt"

## PHAR filenames
PHARfilename="PocketMine-MP.phar"
backupPHARfilename="PocketMine-MP.old"

## PHAR file paths
PHARfilepath="/Users/karldurrance/Documents/MinecraftPE/"
oldPHARfilepath="/Users/karldurrance/Documents/MinecraftPE/" 

## this is your plist file for Launchctl for your Pocketmine server
plistFile="/Users/karldurrance/Library/LaunchAgents/MinecraftPE.plist"

## set this to the max players on your server, this is for the online player check
maxPlayers="20"

## sleep time for post install verification (this should be longer than it takes for your Pocketmine server to start)
sleepTime=30

## binaries (needed if running from launchd)
wgetbin="/usr/local/Cellar/wget/1.19.1/bin/wget"
curlbin="/usr/bin/curl"
jqbin="/usr/local/Cellar/jq/1.5_3/bin/jq"
mcstatusbin="/usr/local/bin/mcstatus"
growlnotifybin="/usr/local/bin/growlnotify"

################################################################################################################

## see if anyone is online, if so then exit - we don't want to kick players for an update
onlineplayers=`"$mcstatusbin" $minecraftIP:$minecraftPort query | grep players | awk '{print $2}'`

if [ "$onlineplayers" = "0/""$maxPlayers" ] || [ -z "$onlineplayers" ]; then
    ## there are no players online, good lets proceed
    echo "No players online, proceeding with update check"

    ## make sure the build state file exists, if not then create it
    if [ ! -f "$stateFile" ]; then
        echo "foo">"$stateFile"
    fi

    ## get the build numbers (existing installed, and latest on Github)
    lastbuildnumber=`cat "$stateFile"`
    latestbuildnumber=`"$curlbin" --silent "$releaseURL" | "$jqbin" --raw-output '.tag_name'`
    downloadURL=`"$curlbin" --silent "$releaseURL" | "$jqbin" --raw-output '.assets[].browser_download_url'`

    echo Current Build:"$lastbuildnumber", Latest Build:"$latestbuildnumber" [URL:"$downloadURL"]

    if [ "$latestbuildnumber" != "$lastbuildnumber" ] && [ ! -z "$latestbuildnumber" ] && [ ! -z "$downloadURL" ]; then
        echo "Build out of date, updating Pocketmine..."

        ## stop Pocketmine
	echo "- Stopping Pocketmine server"
        launchctl unload "$plistFile"

 	## backup the PHAR file in case we need to roll back
	echo "- Backing up existing build"  
	cp "$PHARfilepath""$PHARfilename" "$oldPHARfilepath""$backupPHARfilename"

        ## now download the latest PHAR file
	echo "- Downloading latest build"
	"$wgetbin" -O "$PHARfilepath""$PHARfilename" "$downloadURL"

        ## ok, now restart Pocketmine
        echo "- Starting Pocketmine server"
        launchctl load "$plistFile"

        ## update build number in our state file
        echo $latestbuildnumber>"$stateFile"
        echo "- Updated to build: ""$latestbuildnumber"

        ## send a Growl notification of the update for monitoring purposes
        ## install prowl on your server to push this growl update to your phone too :)
        echo "- Sending Growl notification"
        "$growlnotifybin" -t "Pocketmine Update" -m "Updated to build ""$latestbuildnumber"

        ## now do a post install verification step to ensure we are running, otherwise rollback the change
        echo "- Post install verification, please wait..."
        sleep "$sleepTime"
        ## query the server, make sure that it doesn't return NULL
        postinstallcheck=`"$mcstatusbin" $minecraftIP:$minecraftPort query | grep players | awk '{print $2}'`
        if [ -z "$postinstallcheck" ]; then
             ## we have an issue, Pocketmine is not running, rollback and skip this build
             echo "- Installation failed, rolling back change..."
             ## stop Pocketmine (it probably isn't running anyway, just to be safe)
             echo "- Stopping Pocketmine server..."
             launchctl unload "$plistFile"
		
             ## copy the old PHAR file back
             echo "- Rolling back to previous build"
             cp "$oldPHARfilepath""$backupPHARfilename" "$PHARfilepath""$PHARfilename"

             ## restart Pocketmine
             echo "- Starting Pocketmine server"
             launchctl load "$plistFile"

             ## send a Growl notification saying that the install failed, and we have rolled back
             "$growlnotifybin" -t "Pocketmine Update" -m "Install failed: ""$latestbuildnumber"", ""Rolling back change" 
        else
             echo "- Build update completed successfully, exiting..."
        fi
    else 
        echo "Build up to date, nothing to do - exiting..."
    fi

else
    echo "Active players online, exiting..."
fi
